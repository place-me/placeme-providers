using System;

using Microsoft.Extensions.DependencyInjection;

using NUnit.Framework;

using Placeme.Providers.Azure;
using Placeme.Providers.Interfaces;

namespace Placeme.Providers.Test;

[TestFixture]
public class RemoteProviderFactoryFixture
{
	private IRemoteProviderFactory? _providerFactory;

	[SetUp]
	public void Setup()
	{
		var services = new ServiceCollection();
		services.AddPlacemeProviders();

		var serviceProvider = services.BuildServiceProvider();
		_providerFactory = serviceProvider!.GetService<IRemoteProviderFactory>();
	}

	[Test]
	public void ProvideAzureProvider()
	{
		var provider = _providerFactory!.GetAzureProvider(new AzureConfiguration
		{
			ClientId = Guid.NewGuid().ToString(),
			ClientSecret = Guid.NewGuid().ToString(),
			Tenant = Guid.NewGuid().ToString()
		}, null);
		Assert.IsAssignableFrom(typeof(AzureProvider), provider);
	}

	[Test]
	public void ProvideAzureConfiguration_CanBeDeserialized()
	{
		string data = "{\"Tenant\":\"SuperDuperTenant\",\"ClientId\":\"Secure ClientID\",\"ClientSecret\":\"Even more secure secret!\",\"Scopes\":[\"https://graph.microsoft.com/.default\"],\"RequiredRoles\":[\"User.Read.All\",\"Directory.Read.All\",\"Place.Read.All\"]}";
		var provider = _providerFactory!.GetRemoteProvider(RemoteProviderType.Azure, data, null);
		Assert.IsAssignableFrom(typeof(AzureProvider), provider);
	}
}