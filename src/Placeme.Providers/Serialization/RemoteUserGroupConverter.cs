using System;
using System.Text.Json;
using System.Text.Json.Serialization;

using Placeme.Providers.Interfaces;

namespace Placeme.Providers.Serialization;

public class RemoteUserGroupConverter<TUserGroupImplementation> : JsonConverter<IRemoteUserGroup>
	where TUserGroupImplementation : IRemoteUserGroup
{
	public override bool CanConvert(Type typeToConvert)
	{
		return typeToConvert.IsAssignableTo(typeof(IRemoteUserGroup));
	}

	public override IRemoteUserGroup Read(ref Utf8JsonReader reader, Type typeToConvert,
		JsonSerializerOptions options)
	{
		var internalOptions = new JsonSerializerOptions(options);
		internalOptions.Converters.Clear();
		var userGroup = JsonSerializer.Deserialize<TUserGroupImplementation>(ref reader, internalOptions);
		if (userGroup == null)
		{
			throw new ArgumentNullException(typeof(TUserGroupImplementation).Name, "Cannot deserialize to requested type");
		}

		return userGroup;
	}

	public override void Write(Utf8JsonWriter writer, IRemoteUserGroup value, JsonSerializerOptions options)
	{
		JsonSerializer.Serialize(writer, value, typeof(TUserGroupImplementation));
	}
}