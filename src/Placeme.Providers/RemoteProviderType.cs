namespace Placeme.Providers;

public enum RemoteProviderType
{
	Unknown = 0,
	Azure = 1
}