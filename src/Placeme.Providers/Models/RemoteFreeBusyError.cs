namespace Placeme.Providers.Models;

public class RemoteFreeBusyError
{
	/// <summary>
	/// Describes the error.
	/// </summary>
	public string Message { get; set; } = string.Empty;

	/// <summary>
	/// The response code from querying for the availability of the user, distribution list, or resource.
	/// </summary>
	public string ResponseCode { get; set; } = string.Empty;
}