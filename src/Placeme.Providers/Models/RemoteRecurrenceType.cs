namespace Placeme.Providers.Models;

public enum RemoteRecurrenceType
{
	Daily = 0,
	Weekly = 1,
	AbsoluteMonthly = 2,
	RelativeMonthly = 3,
	AbsoluteYearly = 4,
	RelativeYearly = 5
}