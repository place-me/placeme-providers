using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Microsoft.Graph;
using Microsoft.Identity.Client;

namespace Placeme.Providers.Azure.Authentication;

public class ClientSecretAuthProvider : IAuthenticationProvider
{
	private readonly IConfidentialClientApplication _msalClient;
	private readonly AzureConfiguration _azureConfiguration;

	public ClientSecretAuthProvider(AzureConfiguration configuration)
	{
		_azureConfiguration = configuration;

		_msalClient = ConfidentialClientApplicationBuilder
			.Create(configuration.ClientId)
			.WithClientSecret(configuration.ClientSecret)
			.WithTenantId(configuration.Tenant)
			.Build();
	}

	private async Task<string> GetAccessToken()
	{
		var result = await _msalClient.AcquireTokenForClient(_azureConfiguration.Scopes).ExecuteAsync();
		return result.AccessToken;
	}

	// This is the required function to implement IAuthenticationProvider
	// The Graph SDK will call this function each time it makes a Graph
	// call.
	public async Task AuthenticateRequestAsync(HttpRequestMessage request)
	{
		request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", await GetAccessToken());
	}
}