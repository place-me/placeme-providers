using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

using Microsoft.Graph;

namespace Placeme.Providers.Azure.Authentication;

public class TokenAuthProvider : IAuthenticationProvider
{
	private readonly string _token;

	public TokenAuthProvider(string token)
	{
		_token = token;
	}

	// This is the required function to implement IAuthenticationProvider
	// The Graph SDK will call this function each time it makes a Graph
	// call.
	public Task AuthenticateRequestAsync(HttpRequestMessage request)
	{
		request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _token);
		return Task.CompletedTask;
	}
}