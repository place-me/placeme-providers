using Microsoft.Graph;
using Microsoft.Graph.Extensions;

using Placeme.Providers.Models;
namespace Placeme.Providers.Azure.Mappings;

public static class ScheduleItemMapping
{
	public static RemoteScheduleItem Map(ScheduleItem si)
	{
		return new RemoteScheduleItem
		{
			Start = si.Start.ToDateTime(),
			End = si.End.ToDateTime(),
			Status = AzureEvent.MapFreeBusyStatus(si.Status)
		};
	}
}