﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Graph;
using Microsoft.Graph.Extensions;

using Placeme.Common.Application.Models;
using Placeme.Providers.Azure.Helpers;
using Placeme.Providers.Azure.Mappings;
using Placeme.Providers.Interfaces;
using Placeme.Providers.Models;

using ListRequest = Placeme.Common.Generated.Base.ListRequest;
using Option = Microsoft.Graph.Option;

namespace Placeme.Providers.Azure;

public class AzureProvider : IRemoteProvider
{
	private const int BATCH_SIZE = 50;
	private static readonly JsonSerializerOptions JsonOptions = new() { PropertyNameCaseInsensitive = true };
	private static readonly Option StableIdHeader = new HeaderOption("Prefer", "IdType=\"ImmutableId\"");

	private readonly Expression<Func<User, object>> _requiredUserFields = u => new
	{
		u.Id,
		u.Mail,
		u.GivenName,
		u.Surname,
		u.Department,
		u.StreetAddress,
		u.City,
		u.Country,
		u.MobilePhone,
		u.PostalCode,
		u.MemberOf,
		u.AssignedLicenses
	};

	private readonly GraphServiceClient _graphClient;

	public AzureProvider(GraphServiceClient graphClient)
	{
		_graphClient = graphClient;
	}

	public async Task<IEnumerable<IRemoteUser>> GetUsersAsync(CancellationToken cancellationToken = default)
	{
		var usersRequest = _graphClient.Users.Request()
			.Select(_requiredUserFields)
			.Expand(u => new { u.MemberOf })
			.Top(BATCH_SIZE);

		var placesTask = GetNewPlacesRequest().Select("emailAddress").GetAsync(cancellationToken);
		var usersTask = usersRequest.GetAsync(cancellationToken);

		var placesResult = await placesTask;
		var usersResult = await usersTask;

		// Iterate over all pages and retrive all users.
		// . Placeme will later cache those users, so this endpoint must ensure all users are loaded!
		var allUsers = new List<User>(usersResult);
		var nextPageLink = EntityExtentions.ExtractFromAdditionalData<string, User>(usersResult, "@odata.nextLink");
		while (!string.IsNullOrWhiteSpace(nextPageLink))
		{
			var nextPageRequest = new GraphServiceUsersCollectionRequestBuilder(nextPageLink, _graphClient);
			var nextPage = await nextPageRequest.Request().GetAsync(cancellationToken);
			nextPageLink = EntityExtentions.ExtractFromAdditionalData<string, User>(nextPage, "@odata.nextLink");
			allUsers.AddRange(nextPage);
		}

		var placeMailAddresses = placesResult
									.Select(p => EntityExtentions.ExtractFromAdditionalData<string>(p, "emailAddress"))
									.Where(ma => !string.IsNullOrWhiteSpace(ma)).ToList();
		// Filter rooms and equipment from users
		// . Rooms via the loaded places, equipments via no assigned licenses
		// . This might result in some users missing if they do not have any license applied.
		// . But those users are inactive and therefore not relevant for us?!
		var usersWithoutResources = allUsers.Where(u => !placeMailAddresses.Contains(u.Mail) && u.AssignedLicenses.Any());

		return usersWithoutResources.Select(u =>
		{
			var user = new AzureUser
			{
				Id = u.Id,
				Email = u.Mail,
				Firstname = u.GivenName,
				Lastname = u.Surname,
				Department = u.Department,
				Street = u.StreetAddress,
				City = u.City,
				Country = u.Country,
				PhoneNumber = u.MobilePhone,
				ZipCode = u.PostalCode
			};

			if (u.MemberOf != null)
			{
				user.Groups = u.MemberOf.Select(m =>
				{
					return new AzureUserGroup
					{
						Id = m.Id,
						Name = EntityExtentions.ExtractFromAdditionalData<string>(m, "displayName"),
						Description = EntityExtentions.ExtractFromAdditionalData<string>(m, "description")
					};
				});
			}

			return user;
		});
	}

	public async Task<IEnumerable<RemoteAsset>> GetAssetsAsync(CancellationToken cancellationToken = default)
	{
		var places = await GetNewPlacesRequest().GetAsync(cancellationToken);
		return places.Select(p => PlaceMapping.Map(p));
	}

	private static async Task<IEnumerable<RemoteScheduleInformation>> GetFreeBusyAsync(IUserRequestBuilder userRequestBuilder, IEnumerable<string> attendees, DateTime start,
		DateTime end, int interval = 30, CancellationToken cancellationToken = default)
	{
		var startTime = DateTimeTimeZone.FromDateTime(start);
		var endTime = DateTimeTimeZone.FromDateTime(end);
		if (attendees?.Any() == false)
		{
			return Array.Empty<RemoteScheduleInformation>();
		}

		var res = await userRequestBuilder
			.Calendar
			.GetSchedule(attendees, endTime, startTime, interval)
			.Request()
			.PostAsync(cancellationToken);

		return res.CurrentPage.Select(si => ScheduleInformationMapping.Map(si));
	}

	public Task<IEnumerable<RemoteScheduleInformation>> GetFreeBusyAsync(string userId, IEnumerable<string> attendees, DateTime start, DateTime end,
		int interval = 30, CancellationToken cancellationToken = default)
	{
		return GetFreeBusyAsync(_graphClient.Users[userId], attendees, start, end, interval, cancellationToken);
	}

	public Task<IEnumerable<RemoteScheduleInformation>> GetFreeBusyAsync(IEnumerable<string> attendees, DateTime start, DateTime end,
		int interval = 30, CancellationToken cancellationToken = default)
	{
		return GetFreeBusyAsync(_graphClient.Me, attendees, start, end, interval, cancellationToken);
	}

	public async Task<IRemoteEvent> CreateEventAsync(IRemoteEvent remoteEvent, CancellationToken cancellationToken = default)
	{
		var azureEvent = new Event
		{
			Start = DateTimeTimeZone.FromDateTime(remoteEvent.Start),
			End = DateTimeTimeZone.FromDateTime(remoteEvent.End),
			Body = new ItemBody { Content = remoteEvent.Body, ContentType = BodyType.Html },
			Subject = remoteEvent.Subject,
			Attendees = remoteEvent.Attendees.Select(a => AzureAttendee.Map(a)),
			Recurrence = RecurrenceMapping.Map(remoteEvent.Recurrence, remoteEvent.Start),
			HasAttachments = remoteEvent.HasAttachments,
			Categories = remoteEvent.Categories,
			Sensitivity = MapSensitivity(remoteEvent.Sensitivity),
			ShowAs = MapFreeBusyStatus(remoteEvent.FreeBusyStatus),
			IsReminderOn = remoteEvent.IsReminderOn,
			ReminderMinutesBeforeStart = remoteEvent.ReminderMinutesBeforeStart
		};
		if (remoteEvent.IsOnlineMeeting == true)
		{
			azureEvent.IsOnlineMeeting = true;
			azureEvent.OnlineMeetingProvider = OnlineMeetingProviderType.TeamsForBusiness;
		}

		var res = await _graphClient.Me.Events.Request(new[] { StableIdHeader }).AddAsync(azureEvent, cancellationToken);

		return AzureEvent.Map(res);
	}
	private static FreeBusyStatus MapFreeBusyStatus(RemoteFreeBusyStatus freeBusyStatus)
	{
		return freeBusyStatus switch
		{
			RemoteFreeBusyStatus.Unknown => FreeBusyStatus.Unknown,
			RemoteFreeBusyStatus.Free => FreeBusyStatus.Free,
			RemoteFreeBusyStatus.Busy => FreeBusyStatus.Busy,
			RemoteFreeBusyStatus.OutOfOffice => FreeBusyStatus.Oof,
			RemoteFreeBusyStatus.WorkingElsewhere => FreeBusyStatus.WorkingElsewhere,
			RemoteFreeBusyStatus.Tentative => FreeBusyStatus.Tentative,
			_ => FreeBusyStatus.Unknown
		};
	}
	private static Sensitivity MapSensitivity(RemoteSensitivity sensitivity)
	{
		return sensitivity switch
		{
			RemoteSensitivity.Normal => Sensitivity.Normal,
			RemoteSensitivity.Personal => Sensitivity.Personal,
			RemoteSensitivity.Private => Sensitivity.Private,
			RemoteSensitivity.Confidential => Sensitivity.Confidential,
			_ => Sensitivity.Normal
		};
	}

	public async Task<IRemoteEvent> GetEventAsync(string id, CancellationToken cancellationToken = default)
	{
		var res = await _graphClient.Me
			.Events[id]
			.Request(new[] { StableIdHeader })
			.Select(e => new
			{
				e.Id,
				e.Start,
				e.End,
				e.Body,
				e.BodyPreview,
				e.Subject,
				e.Attendees,
				e.Recurrence,
				e.HasAttachments,
				e.Sensitivity,
				e.Categories,
				e.ShowAs,
				e.IsOnlineMeeting,
				e.IsReminderOn,
				e.ReminderMinutesBeforeStart,
				e.Organizer
			})
			.GetAsync(cancellationToken);

		return AzureEvent.Map(res);
	}

	public async Task<PaginatedList<IRemoteEvent>> ListEventsAsync(ListRequest listRequest, CancellationToken cancellationToken = default)
	{
		var requestOptions = listRequest?.ConvertToPagingQueryOptions() ?? new Collection<Option>();
		requestOptions.Add(StableIdHeader);

		var response = await _graphClient.Me.Events
		.Request(requestOptions)
		.Filter(listRequest?.ConvertToFilterString<Event>())
		.OrderBy(listRequest?.ConvertToSortString<Event>())
		.GetAsync(cancellationToken);

		var events = response.CurrentPage.Select(e => AzureEvent.Map(e));

		var totalCount = EntityExtentions.ExtractFromAdditionalData<int>(response.AdditionalData, "@odata.count");
		return new PaginatedList<IRemoteEvent>(events, totalCount, listRequest?.PageNumber > 0 ? listRequest.PageNumber : 1, listRequest?.PageSize ?? totalCount);
	}

	public async Task<IRemoteEvent> UpdateEventAsync(IRemoteEvent remoteEvent, RemoteEventUpdatableProperties propertiesToUpdate,
		CancellationToken cancellationToken = default)
	{
		var existingEvent = await _graphClient.Me
			.Events[remoteEvent.Id]
			.Request()
			.GetAsync(cancellationToken);

		if (existingEvent == null)
		{
			throw new ArgumentException($"Could not update event, event {remoteEvent.Id} does not exist");
		}

		if (propertiesToUpdate == RemoteEventUpdatableProperties.None)
		{
			return AzureEvent.Map(existingEvent);
		}

		var updatedEvent = new Event();
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.Start))
		{
			updatedEvent.Start = DateTimeTimeZone.FromDateTime(remoteEvent.Start, TimeZoneInfo.Utc.Id);
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.End))
		{
			updatedEvent.End = DateTimeTimeZone.FromDateTime(remoteEvent.End, TimeZoneInfo.Utc.Id);
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.Body))
		{
			updatedEvent.Body = new ItemBody { Content = remoteEvent.Body, ContentType = BodyType.Html };
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.Subject))
		{
			updatedEvent.Subject = remoteEvent.Subject;
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.Attendees))
		{
			updatedEvent.Attendees = remoteEvent.Attendees.Select(a => AzureAttendee.Map(a));
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.Recurrence))
		{
			updatedEvent.Recurrence = RecurrenceMapping.Map(remoteEvent.Recurrence, existingEvent.Start.ToDateTime());
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.Categories))
		{
			updatedEvent.Categories = remoteEvent.Categories;
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.Sensitivity))
		{
			updatedEvent.Sensitivity = MapSensitivity(remoteEvent.Sensitivity);
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.FreeBusyStatus))
		{
			updatedEvent.ShowAs = MapFreeBusyStatus(remoteEvent.FreeBusyStatus);
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.IsOnlineMeeting) && remoteEvent.IsOnlineMeeting == true)
		{
			updatedEvent.IsOnlineMeeting = true;
			updatedEvent.OnlineMeetingProvider = OnlineMeetingProviderType.TeamsForBusiness;
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.IsReminderOn))
		{
			updatedEvent.IsReminderOn = remoteEvent.IsReminderOn;
			if (updatedEvent.IsReminderOn == false)
			{
				updatedEvent.ReminderMinutesBeforeStart = 0;
			}
		}
		if (propertiesToUpdate.HasFlag(RemoteEventUpdatableProperties.ReminderMinutesBeforeStart))
		{
			updatedEvent.ReminderMinutesBeforeStart = remoteEvent.ReminderMinutesBeforeStart;
		}

		var res = await _graphClient.Me
			.Events[remoteEvent.Id]
			.Request(new[] { StableIdHeader })
			.UpdateAsync(updatedEvent, cancellationToken);

		return AzureEvent.Map(res);
	}

	public async Task DeleteEventAsync(string id, CancellationToken cancellationToken = default)
	{
		await _graphClient.Me.Events[id]
			.Request(new[] { StableIdHeader })
			.DeleteAsync(cancellationToken);
	}

	public Task<bool> ValidateConfigurationAsync(string configurationJson,
		CancellationToken cancellationToken = default)
	{
		var config = JsonSerializer.Deserialize<AzureConfiguration>(configurationJson, JsonOptions);
		if (config == null)
		{
			return Task.FromResult(false);
		}

		if (string.IsNullOrWhiteSpace(config.ClientId))
		{
			return Task.FromResult(false);
		}

		if (string.IsNullOrWhiteSpace(config.ClientSecret))
		{
			return Task.FromResult(false);
		}

		if (string.IsNullOrWhiteSpace(config.Tenant))
		{
			return Task.FromResult(false);
		}

		return Task.FromResult(true);
	}

	public Task<IEnumerable<IRemotePermission>> ReviewPermissionsAsync(CancellationToken cancellationToken = default)
	{
		// TODO -> re-implement after refactoring

		// var accessToken = await (_graphClient.AuthenticationProvider as AppSecretAuthProvider).GetAccessToken();
		// var jwt = new JsonWebToken(accessToken);
		// if (jwt.TryGetClaim("roles", out var claim) && claim.ValueType == JsonClaimValueTypes.JsonArray)
		// {
		// 	var roles = JsonSerializer.Deserialize<string[]>(claim.Value, _jsonOptions);
		// 	return _config.RequiredRoles.Select(r => new AzurePermission
		// 	{
		// 		Granted = roles.Contains(r),
		// 		Identifier = r
		// 	});
		// }
		return Task.FromResult<IEnumerable<IRemotePermission>>(new List<IRemotePermission>());
	}

	public Task<string> GenerateDefaultConfigurationAsync(string configurationJson,
		CancellationToken cancellationToken = default)
	{
		var config = JsonSerializer.Deserialize<AzureConfiguration>(configurationJson, JsonOptions);
		if (config == null)
		{
			throw new ArgumentNullException(nameof(AzureConfiguration), "Unable to deserialize configJson to AzureConfiguration");
		}

		config.Scopes = AzureConfiguration.DefaultScopes;
		config.RequiredRoles = AzureConfiguration.DefaultRoles;

		return Task.FromResult(JsonSerializer.Serialize(config, JsonOptions));
	}

	private IGraphServicePlacesCollectionRequest GetNewPlacesRequest()
	{
		var url = _graphClient.Places.AppendSegmentToRequestUrl("microsoft.graph.room");
		return new GraphServicePlacesCollectionRequestBuilder(url, _graphClient).Request().Top(10000);
	}

	public async Task<byte[]> GetPhotoAsync(string id, CancellationToken cancellationToken = default)
	{
		var photo = await _graphClient.Users[id].Photo.Content.Request().GetAsync(cancellationToken);
		if (photo == null)
		{
			return Array.Empty<byte>();
		}

		byte[] bytes;
		using (var memStream = new MemoryStream())
		{
			await photo!.CopyToAsync(memStream!, cancellationToken);
			bytes = memStream.ToArray();
		}
		return bytes;
	}
}