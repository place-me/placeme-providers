using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Graph;
using Microsoft.Graph.Extensions;

using Placeme.Providers.Azure.Mappings;
using Placeme.Providers.Interfaces;
using Placeme.Providers.Models;

namespace Placeme.Providers.Azure;

public class AzureEvent : IRemoteEvent
{
	private ICollection<IRemoteAttendee> _attendees = new List<IRemoteAttendee>();

	public string? Id { get; set; }
	public string? Subject { get; set; }
	public string? Body { get; set; }
	public string? BodyPreview { get; set; }
	public DateTime Start { get; set; }
	public DateTime End { get; set; }
	public RemoteRecurrencePattern? Recurrence { get; set; }

	public IEnumerable<IRemoteAttendee> Attendees
	{
		get => _attendees;
		set => _attendees = value.ToList();
	}
	public IEnumerable<string> Categories { get; set; } = new List<string>();
	public RemoteSensitivity Sensitivity { get; set; }
	public RemoteFreeBusyStatus FreeBusyStatus { get; set; }
	public bool? IsOnlineMeeting { get; set; }
	public bool? HasAttachments { get; set; }
	public bool? IsReminderOn { get; set; }
	public int? ReminderMinutesBeforeStart { get; set; }
	public IRemoteAttendee? Organizer { get; set; }
	public bool? IsOrganizer { get; set; }

	public void AddAttendee(Attendee attendee)
	{
		_attendees.Add(new AzureAttendee
		{
			DisplayName = attendee.EmailAddress.Name,
			Email = attendee.EmailAddress.Address,
			Type = MapType(attendee.Type),
			State = MapState(attendee.Status)
		});
	}

	public static AzureEvent Map(Event graphEvent)
	{
		var azureEvent = new AzureEvent
		{
			Id = graphEvent.Id,
			Start = graphEvent.Start.ToDateTime(),
			End = graphEvent.End.ToDateTime(),
			Body = graphEvent.Body.Content,
			BodyPreview = graphEvent.BodyPreview,
			Subject = graphEvent.Subject,
			Recurrence = RecurrenceMapping.Map(graphEvent.Recurrence),
			Categories = graphEvent.Categories,
			Sensitivity = MapSensitivity(graphEvent.Sensitivity),
			FreeBusyStatus = MapFreeBusyStatus(graphEvent.ShowAs),
			HasAttachments = graphEvent.HasAttachments,
			IsOnlineMeeting = graphEvent.IsOnlineMeeting,
			IsReminderOn = graphEvent.IsReminderOn,
			ReminderMinutesBeforeStart = graphEvent.ReminderMinutesBeforeStart,
			IsOrganizer = graphEvent.IsOrganizer
		};

		if (graphEvent.Organizer != null)
		{
			azureEvent.Organizer = new AzureAttendee { DisplayName = graphEvent.Organizer.EmailAddress.Name, Email = graphEvent.Organizer.EmailAddress.Address };
		}
		foreach (var attendee in graphEvent.Attendees)
		{
			azureEvent.AddAttendee(attendee);
		}

		return azureEvent;
	}
	private static RemoteSensitivity MapSensitivity(Sensitivity? sensitivity)
	{
		return sensitivity switch
		{
			Microsoft.Graph.Sensitivity.Normal => RemoteSensitivity.Normal,
			Microsoft.Graph.Sensitivity.Personal => RemoteSensitivity.Personal,
			Microsoft.Graph.Sensitivity.Private => RemoteSensitivity.Private,
			Microsoft.Graph.Sensitivity.Confidential => RemoteSensitivity.Confidential,
			_ => RemoteSensitivity.Normal,
		};
	}

	internal static RemoteFreeBusyStatus MapFreeBusyStatus(FreeBusyStatus? azureStatus)
	{
		return azureStatus switch
		{
			Microsoft.Graph.FreeBusyStatus.Unknown => RemoteFreeBusyStatus.Unknown,
			Microsoft.Graph.FreeBusyStatus.Free => RemoteFreeBusyStatus.Free,
			Microsoft.Graph.FreeBusyStatus.Busy => RemoteFreeBusyStatus.Busy,
			Microsoft.Graph.FreeBusyStatus.Oof => RemoteFreeBusyStatus.OutOfOffice,
			Microsoft.Graph.FreeBusyStatus.WorkingElsewhere => RemoteFreeBusyStatus.WorkingElsewhere,
			Microsoft.Graph.FreeBusyStatus.Tentative => RemoteFreeBusyStatus.Tentative,
			_ => RemoteFreeBusyStatus.Unknown,
		};
	}

	private static RemoteAttendeeType MapType(AttendeeType? attendeeType)
	{
		return attendeeType switch
		{
			AttendeeType.Required => RemoteAttendeeType.Required,
			AttendeeType.Optional => RemoteAttendeeType.Optional,
			AttendeeType.Resource => RemoteAttendeeType.Resource,
			_ => RemoteAttendeeType.Unknown,
		};
	}

	private static RemoteAttendeeResponseState MapState(ResponseStatus responseStatus)
	{
		return responseStatus.Response switch
		{
			ResponseType.Accepted => RemoteAttendeeResponseState.Accepted,
			ResponseType.Declined => RemoteAttendeeResponseState.Declined,
			ResponseType.None => RemoteAttendeeResponseState.None,
			ResponseType.NotResponded => RemoteAttendeeResponseState.NotResponded,
			ResponseType.Organizer => RemoteAttendeeResponseState.Organizer,
			ResponseType.TentativelyAccepted => RemoteAttendeeResponseState.TentativelyAccepted,
			_ => throw new ArgumentOutOfRangeException(nameof(responseStatus), responseStatus.Response, "An unexpected ResponseStatus was received"),
		};
	}
}