namespace Placeme.Providers.Interfaces;

public interface IRemotePermission
{
	string? Identifier { get; set; }
	string? Description { get; set; }
	bool Granted { get; set; }
}