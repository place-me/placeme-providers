using System;

namespace Placeme.Providers.Interfaces;

[Flags]
public enum RemoteEventUpdatableProperties
{
	None = 0,
	Start = 1,
	End = 2,
	Subject = 4,
	Body = 8,
	Attendees = 16,
	Recurrence = 32,
	Sensitivity = 64,
	Categories = 128,
	FreeBusyStatus = 256,
	IsOnlineMeeting = 512,
	IsReminderOn = 1024,
	ReminderMinutesBeforeStart = 2048
}