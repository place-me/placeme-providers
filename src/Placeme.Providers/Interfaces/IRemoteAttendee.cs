namespace Placeme.Providers.Interfaces;

public interface IRemoteAttendee
{
	string? DisplayName { get; set; }
	string? Email { get; set; }

	RemoteAttendeeType Type { get; set; }

	RemoteAttendeeResponseState State { get; set; }
}