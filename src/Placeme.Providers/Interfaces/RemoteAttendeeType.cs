namespace Placeme.Providers.Interfaces;

public enum RemoteAttendeeType
{
	Unknown = 0,
	Required = 1,
	Optional = 2,
	Resource = 3
}