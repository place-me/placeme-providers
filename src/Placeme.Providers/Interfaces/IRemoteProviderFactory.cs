using Placeme.Providers.Azure;

namespace Placeme.Providers.Interfaces;

public interface IRemoteProviderFactory
{
	IRemoteProvider GetAzureProvider(AzureConfiguration azureConfig, string? accessToken);
	IRemoteProvider GetNoopProvider();
	IRemoteProvider GetRemoteProvider(RemoteProviderType providerType, string target, string? accessToken);
}